package com.example.demo.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserService {
    @Autowired
    public UserRepository userRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    public UserDao createOrUpdateUser(UserDao user) throws Exception{
        if(Objects.nonNull(this.getUserByEmail(user.getEmail()))){
            throw new Exception();
        }else{
            UserDao newUser = new UserDao();
            newUser.setEmail(user.getEmail());
            newUser.setFirstName(user.getFirstName());
            newUser.setMiddleName(user.getMiddleName());
            newUser.setLastName(user.getLastName());
            newUser.setBirthday(user.getBirthday());
            newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
            newUser.setStatus("Active");
            return userRepository.save(newUser);
        }
    }
    public UserDao createOrUpdateUser(UserDao user, Long id) throws Exception {
        UserDao existingUser = this.getUserById(id);
        if(existingUser.getStatus().equals("Deleted")){
            throw new Exception();
        }else{
            existingUser.setEmail(user.getEmail());
            existingUser.setFirstName(user.getFirstName());
            existingUser.setMiddleName(user.getMiddleName());
            existingUser.setLastName(user.getLastName());
            existingUser.setBirthday(user.getBirthday());
            existingUser.setPassword(bcryptEncoder.encode(user.getPassword()));
            existingUser.setStatus(user.getStatus());
            return userRepository.save(existingUser);
        }
    }

    public UserDao getUserById(Long id) {
        return userRepository.findById(id).get();
    }
    public UserDao getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }
    public Iterable<UserDao> getAllUsers() {
        return userRepository.getActiveUsers();
    }
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }
}
