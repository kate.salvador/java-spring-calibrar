package com.example.demo.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController //di nagwwork pag @Controller lang
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;

    //@GetMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.GET).
    @RequestMapping("/greeting") //default method is get, lagay mo otherwise (see @GetMapping, also works)
    public String getGreeting(@RequestParam(value = "name", defaultValue = "World!!") String name) {
        return String.format("Hello %s!!", name);
    }

    @PostMapping("/create")
    public UserDao createUser(@RequestBody UserDao user) throws Exception {
        try{
            return userService.createOrUpdateUser(user);
        }catch (Exception e){
            throw new Exception("Email is already in use!");
        }
    }

    @GetMapping("/list")
    public Iterable<UserDao> getUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/find/{id}")
    public UserDao findUserById(@PathVariable Long id) throws Exception {
        try{
            return userService.getUserById(id);
        }catch(Exception e){
            throw new Exception("User not found with id: " + id);
        }
    }

    @PutMapping("/update/{id}")
    public UserDao updateUser(@RequestBody UserDao user, @PathVariable Long id) throws Exception {
        try{
            return userService.createOrUpdateUser(user, id);
        }catch (Exception e){
            throw new Exception("User not found with id: " + id);
        }
    }

    @DeleteMapping("/delete/{id}")
    public String deleteUser(@PathVariable Long id) throws Exception {
        try{
            userService.deleteUserById(id);
            return ("Successfully deleted user with id: " + id);
        }catch(Exception e){
            throw new Exception("User not found with id: " + id);
        }
    }

}

