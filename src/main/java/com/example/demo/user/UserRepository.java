package com.example.demo.user;

import com.example.demo.user.UserDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserDao, Long> {
//    UserDao findByUsername(String email);

    @Query(value = "select u from UserDao u where u.email = ?1")
    UserDao findByEmail(String email);

    @Query(value = "select * from user where status = 'Active'", nativeQuery = true)
    Iterable<UserDao> getActiveUsers();
}
