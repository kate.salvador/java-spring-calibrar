package com.example.demo.login.service;

import java.util.ArrayList;

import com.example.demo.user.UserDao;
import com.example.demo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDao user = userService.getUserByEmail(username);
        System.out.print("user " + user);
        if (user == null || user.getStatus().equals("Deleted")) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }else{
            return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                    new ArrayList<>());
        }
    }
}